import 'package:flutter/cupertino.dart';
import 'package:horsecal/models/user.dart';

class Horse {
  final String name;
  final String birthdate;
  final User owner;
  final String url;

  Horse({
    @required this.name,
    @required this.birthdate,
    @required this.owner,
    @required this.url,
  });
}
