import 'package:flutter/cupertino.dart';
import 'package:horsecal/models/horse.dart';
import 'package:horsecal/models/user.dart';

class Requests {
  final User user;
  final Horse horse;

  Requests({
    @required this.user,
    @required this.horse,
  });
}
