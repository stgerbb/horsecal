import 'package:flutter/material.dart';
import 'package:horsecal/models/user.dart';
import 'package:horsecal/widgets/widget_app_bar.dart';

class CalendarMembersWidget extends StatefulWidget {
  final String heading;

  CalendarMembersWidget({@required this.heading});

  @override
  _CalendarMembersWidgetState createState() => _CalendarMembersWidgetState();
}

class _CalendarMembersWidgetState extends State<CalendarMembersWidget> {
  List<User> _members = [
    new User(
        firstName: 'Anna',
        lastName: 'Müller',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Rebekka',
        lastName: 'Gratwohl',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
    new User(
        firstName: 'Johnny',
        lastName: 'Seiler',
        email: 'email@email.com',
        password: 'test1234'),
  ];

  @override
  void initState() {
    super.initState();
  }

  Widget _listItemBuilder(BuildContext context, int index) {
    return Container(
      height: 85.0,
      child: Card(
        child: Row(
          children: <Widget>[
            SizedBox(width: 16.0),
            Expanded(
              child: Text('${_members[index].firstName} ${_members[index].lastName}', textAlign: TextAlign.start),
            ),
            IconButton(
              icon: Icon(Icons.close),
              color: Colors.grey[600],
              onPressed: () => showDialog(context: context, builder: (context) => _dialogBuilder(context, _members[index])),
            ),
          ],
        ),
      ),
    );
  }

  Widget _dialogBuilder(BuildContext context, User user) {
    return SimpleDialog(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text(
              '${user.firstName} ${user.lastName} aus Kalender entfernen?',
              style: Theme.of(context).textTheme.subhead),
        ),
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
                child: Text('Abbrechen'),
                textColor: Colors.grey[600],
                onPressed: () =>
                    Navigator.pop(context, false)),
            RaisedButton(
              child: Text('Entfernen'),
              color: Colors.red[400],
              onPressed: () {},
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
          title: 'Reitbeteiligungen',
          leading: true,
          setting: true,
          notification: true),
      body: ListView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        itemCount: _members.length,
        itemBuilder: _listItemBuilder,
      ),
    );
  }
}