import 'package:flutter/material.dart';
import 'package:horsecal/models/horse.dart';
import 'package:horsecal/models/user.dart';
import 'package:horsecal/screens/widget_add_edit_horse.dart';
import 'package:horsecal/screens/widget_calendar.dart';
import 'package:horsecal/widgets/widget_app_bar.dart';

class HomeWidget extends StatefulWidget {

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  List<Horse> _horses = [
    new Horse(
        name: "Jolly Jumper",
        birthdate: "01.01.2010",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse1.png"),
    new Horse(
        name: "Agon",
        birthdate: "01.01.2011",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse2.png"),
    new Horse(
        name: "Jacqueline",
        birthdate: "01.01.2012",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse3.png"),
    new Horse(
        name: "Jacqueline",
        birthdate: "01.01.2012",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse3.png"),
    new Horse(
        name: "Jacqueline",
        birthdate: "01.01.2012",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse3.png"),
    new Horse(
        name: "Jacqueline",
        birthdate: "01.01.2012",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse3.png"),
    new Horse(
        name: "Jacqueline",
        birthdate: "01.01.2012",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse3.png"),
    new Horse(
        name: "Jacqueline",
        birthdate: "01.01.2012",
        owner: new User(
            firstName: 'Bettina',
            lastName: 'Morgenthaler',
            email: 'email@email.com',
            password: 'test1234'),
        url: "horse3.png")
  ];

  @override
  void initState() {
    super.initState();
  }

  Widget _buildSearchBar(BuildContext context) {
    return SafeArea(
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search),
                hintText: 'Pferdname oder Pferdbesitzer...',
              ),
            ),
          ),
          SizedBox(width: 8.0),
          IconButton(
            icon: Icon(Icons.calendar_today),
            color: Colors.grey[600],
            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CalendarWidget(heading: 'Übersicht', icon: false))),
          ),
        ],
      ),
    );
  }

  Widget _listItemBuilder(BuildContext context, int index) {
    return Container(
      height: 85.0,
      child: GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CalendarWidget(
              heading: _horses[index].name,
              icon: true,
            ),
          ),
        ),
        child: Card(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 8.0,
                    ),
                    Container(
                      height: 62,
                      width: 62,
                      child: CircleAvatar(
                        backgroundImage: AssetImage(
                          'assets/images/${_horses[index].url}',
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(_horses[index].name),
                        SizedBox(
                          height: 4.0,
                        ),
                        Text(
                          '${_horses[index].owner.firstName} ${_horses[index].owner.lastName}',
                          style:
                          Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                icon: Icon(Icons.edit),
                color: Colors.grey[600],
                onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddEditHorseWidget(
                      title: 'Pferd bearbeiten',
                      button: 'Speichern',
                    ),
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.delete),
                color: Colors.grey[600],
                onPressed: () => showDialog(context: context, builder: (context) => _dialogBuilder(context, _horses[index])),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _dialogBuilder(BuildContext context, Horse horse) {
    return SimpleDialog(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(16.0),
          child: Text('${horse.name} löschen?', style: Theme.of(context).textTheme.subhead),
        ),
        Row(
          mainAxisAlignment:
          MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            FlatButton(
              child: Text('Abbrechen'),
              textColor: Colors.grey[600],
              onPressed: () => Navigator.pop(context, false),
            ),
            RaisedButton(
              child: Text('Löschen'),
              color: Colors.red[400],
              onPressed: () {

              },
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        title: 'HorseCal',
        leading: false,
        notification: true,
        setting: true,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AddEditHorseWidget(title: 'Pferd erstellen', button: 'Erstellen'))),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: Column(
          children: <Widget>[
            _buildSearchBar(context),
            Expanded(
              child: ListView.builder(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                shrinkWrap: true,
                itemCount: _horses.length,
                itemBuilder: _listItemBuilder,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
