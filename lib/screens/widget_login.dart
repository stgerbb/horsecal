import 'package:flutter/material.dart';
import 'package:horsecal/screens/widget_forgot_password.dart';
import 'package:horsecal/screens/widget_home.dart';
import 'package:horsecal/screens/widget_register.dart';
import 'package:horsecal/widgets/widget_entry_email.dart';
import 'package:horsecal/widgets/widget_entry_password.dart';

class LoginWidget extends StatelessWidget {

  Widget _buildImage(BuildContext context) {
    return SafeArea(
      child: Image(
        image: AssetImage('assets/images/logo_placeholder.png'),
        height: 192.0,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Text('HORSECAL',
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.headline,
    );
  }

  Widget _buildTextFields() {
    return Column(
      children: <Widget>[
        EmailEntry(),
        SizedBox(height: 8.0),
        PasswordEntry(hint: 'Passwort'),
      ],
    );
  }

  Widget _buildForgotPasswordButton(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.all(0.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text('Passwort vergessen?', style: Theme.of(context).textTheme.body1),
      ),
      onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPasswordWidget())),
    );
  }

  Widget buildButtons(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text('Anmelden'),
          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeWidget())),
        ),
        RaisedButton(
          child: Text('Registrieren'),
          color: Colors.lightGreen[400],
          textColor: Colors.white,
          onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterWidget())),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _buildImage(context),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
              child: Column(
                children: <Widget>[
                  SizedBox(height: 16.0),
                  _buildTitle(context),
                  SizedBox(height: 32.0),
                  _buildTextFields(),
                  _buildForgotPasswordButton(context),
                  buildButtons(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}