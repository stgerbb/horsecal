import 'package:flutter/material.dart';
import 'package:horsecal/widgets/widget_entry_email.dart';
import 'package:horsecal/widgets/widget_entry_name.dart';
import 'package:horsecal/widgets/widget_entry_password.dart';

class RegisterWidget extends StatelessWidget {

  Widget _buildTextFields() {
    return Column(
      children: <Widget>[
        SafeArea(
          child: NameEntry(name: 'Vorname'),
        ),
        SizedBox(height: 8.0),
        NameEntry(name: 'Nachname'),
        SizedBox(height: 8.0),
        EmailEntry(),
        SizedBox(height: 8.0),
        PasswordEntry(hint: 'Passwort'),
        SizedBox(height: 8.0),
        PasswordEntry(hint: 'Passwort bestätigen'),
      ],
    );
  }

  Widget _buildRegisterButton(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        RaisedButton(
          child: Text('Konto erstellen'),
          onPressed: () => Navigator.pop(context, false),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrieren'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
        child: Column(
          children: <Widget>[
            _buildTextFields(),
            SizedBox(height: 8.0),
            _buildRegisterButton(context),
          ],
        ),
      ),
    );
  }
}
