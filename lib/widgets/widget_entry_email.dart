import 'package:flutter/material.dart';

class EmailEntry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        hintText: 'Email-Adresse',
      ),
    );
  }
}
