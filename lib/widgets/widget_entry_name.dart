import 'package:flutter/material.dart';

class NameEntry extends StatelessWidget {
  final String name;

  NameEntry({
    @required this.name,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(0),
        ),
        hintText: name,
      ),
    );
  }
}
